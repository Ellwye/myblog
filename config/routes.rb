Rails.application.routes.draw do
  resources :commentaires
  resources :tags
  resources :blogs
  resources :users
  # resources :sessions, only: [:new, :create, :destroy] façon différentes d'écrire
  resources :sessions, only: %w[new create destroy]
  root "blogs#index"

  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'profile', to: 'users#profile', as: 'profile'

  namespace :admin do
    resources :commentaires
    resources :tags
    resources :blogs
    resources :users
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
