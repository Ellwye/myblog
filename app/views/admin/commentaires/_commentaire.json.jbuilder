json.extract! commentaire, :id, :content, :published_at, :name, :email, :blog_id, :created_at, :updated_at
json.url commentaire_url(commentaire, format: :json)
