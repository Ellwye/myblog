# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Commentaire.delete_all if Commentaire.all.size.positive? # En premier car le commentaire dépend d'un blog, on ne peut donc pas supprimer blog avant commentaire
Blog.delete_all if Blog.all.size.positive? # En deuxième car le blog dépend d'un user, on ne peut donc pas supprimer user avant blog
User.delete_all if User.all.size.positive?
Tag.delete_all if Tag.all.size.positive?

# Création de l'user en premier, il va servir pour créer le blog
(1..30).each do |number|
  User.create  name:   Faker::Name.name,
                      email:  Faker::Internet.email
end

# Création du blog en deuxième car il dépend d'un user
(1..100).each do |number|
  Blog.create  title:        Faker::Lorem.sentence,
                      content:      Faker::Lorem.paragraph(sentence_count: 5),
                      published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
                      draft:        rand(0..1),
                      user:         User.all.sample
end

(1..10).each do |number|
  Tag.create name:   Faker::Lorem.word
end

# Création du commentaire en troisième car il dépend d'un blog
(1..100).each do |number|
  Commentaire.create  content: Faker::Lorem.paragraph(sentence_count: 5),
                                    published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
                                    name: Faker::Name.name,
                                    email: Faker::Internet.email,
                                    blog: Blog.all.sample
end
