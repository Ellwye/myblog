class CreateCommentaires < ActiveRecord::Migration[6.0]
  def change
    create_table :commentaires do |t|
      t.text :content
      t.datetime :published_at
      t.string :name
      t.string :email
      t.references :blog, null: false, foreign_key: true

      t.timestamps
    end
    add_index :commentaires, :published_at
    add_index :commentaires, :name
    add_index :commentaires, :email
  end
end
